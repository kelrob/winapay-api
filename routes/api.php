<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::post('forgot-password', 'UserController@forgotPassword');

Route::group(['middleware' => 'auth:api'], function() {

    # Users Routes
    Route::get('user', 'UserController@loggedInUser');
    Route::post('change-avatar', 'UserController@changeAvatar');
    Route::post('increase-wallet', 'UserController@increaseUserWallet');
    Route::post('update-profile', 'UserController@updateUser');
    Route::get('referral-link', 'UserController@referralLink');

    # Games Routes
    Route::get('games/{game_id}', 'GamesController@getGame');
    Route::get('user/games/{user_id}', 'GamesController@userGames');
    Route::get('all-games', 'GamesController@allGames');
    Route::get('all-games/free', 'GamesController@allFreeGames');
    Route::get('all-games/paid', 'GamesController@allPaidGames');
    Route::get('increase-game-view/{id}', 'GamesController@increaseGameView');
    Route::post('game-play', 'GamesController@userPlayedGame');
    Route::post('create-game', 'GamesController@createGame');
    Route::get('total-play/{game_id}', 'GamesController@totalPlay');
    Route::get('search-game/{search_term}', 'GamesController@searchGame');
    Route::get('games-played/{user_id}', 'GamesController@gamesPlayed');
    Route::post('record-game-played', 'GamesController@recordGameScore');
    Route::get('close-game/{game_id}', 'GamesController@closeGame');
    Route::get('open-game/{game_id}', 'GamesController@openGame');
    Route::post('edit-game/{game_id}', 'GamesController@editGame');


    # Payment Route
    Route::post('record-deposit', 'UserController@newDeposit');

    # LeaderBoard Route
    Route::get('daily-leaderboard', 'LeaderboardController@getDailyLeaderBoard');
    Route::get('monthly-leaderboard', 'LeaderboardController@getMonthlyLeaderBoard');

    # Point Converter
    Route::post('point-converter', 'PointConverterController@convertPoint');

    # Subscription Route
    Route::post('subscribe-user', 'SubscriptionController@activateSubscription');
    Route::get('sub-active/{user_id}', 'SubscriptionController@subActive');

    # Withdrawal and Bank Related
    Route::post('withdrawal-request', 'UserController@processWithdrawal');
    Route::post('verify-bank-details', 'BankDetailsController@verifyBankDetails');
    Route::get('bank-details-uploaded/{user_id}', 'BankDetailsController@checkExist');
    Route::post('submit-bank-details', 'BankDetailsController@newBankDetails');
    Route::get('bank-details-verified/{user_id}', 'BankDetailsController@bankDetailsVerified');
    Route::get('pending-withdrawal-request/{user_id}', 'BankDetailsController@userHasPendingWithdrawal');
    Route::get('bank-details-approved', 'BankDetailsController@approveBankDetails');
});
