<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnattendedUsers extends Model
{
    protected $table = 'unattended_users';
    protected $guarded = ['id'];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;
}
