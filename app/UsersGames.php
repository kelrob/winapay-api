<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersGames extends Model
{
    protected $table = 'users_games';
    protected $guarded = ['id'];
    public $timestamps = false;

    const CREATED_AT = 'time_created';
    const UPDATED_AT = 'time_updated';
}
