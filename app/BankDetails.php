<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    protected $table = 'bank_details2';
    protected $guarded = ['id'];

    const CREATED_AT = null;
    const UPDATED_AT = null;
}
