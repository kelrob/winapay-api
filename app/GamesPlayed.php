<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamesPlayed extends Model
{
    protected $table = 'game_played';
    protected $guarded = ['id'];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;
}
