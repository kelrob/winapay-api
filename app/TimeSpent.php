<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSpent extends Model
{
    protected $table = 'time_spent';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    const CREATED_AT = null;
    const UPDATED_AT = null;
}
