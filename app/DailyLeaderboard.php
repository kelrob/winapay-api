<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyLeaderboard extends Model
{
    protected $table = 'daily_leaderboard';
    protected $guarded = ['id'];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;
}
