<?php

namespace App\Http\Controllers;

use App\User;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{

    public function subActive($userId) {
        $query = Subscription::where('user_id', $userId);
        $countResult = $query->count();

        if ($countResult > 0) {
            return response()->json([
               'status' => 'success',
               'active' => true
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'active' => false
            ]);
        }
    }

    public function activateSubscription(Request $request) {
        $user = Auth::user();

        $koboAmount = $request->kobo_amount;
        $userRef = $user->my_ref;

        if ($this->subActive($user->id)->getData()->active == false) {
            if ($koboAmount == 10099) {
                if ($userRef != 0) {
                    $userRef = User::find($userRef);

                    $refPhone = $userRef->phone;
                    $refName = $userRef->fullname;

                    # Referer Current Point and Update New Points
                    $refPoints = $userRef->current_points;
                    $userRef->current_points = $refPoints + 100;

                    # Get referer Points earned by Inviting
                    $invPoints = $userRef->points_earned_by_inv;
                    $userRef->points_earned_by_inv = $invPoints + 100;

                    # Save the Updated Records
                    $userRef->save();

                    # Notify Referer
                    $message = "Hello $refName, your Referral just made a boost and you have received 100 point.";
                    _sendSmsNotification($refPhone, $message);
                }

                # Subscribe the User
                $sub = new Subscription;
                $sub->user_id = $user->id;
                $sub->day_count = 1;
                $sub->day_count_expired = 2;
                $sub->save();

                return response()->json(
                    [
                        'status' => 'success',
                        'response' => 'Subscribed Successfully',
                        'days' => 2
                    ]
                );

            } else if ($koboAmount == 60099) {
                if ($userRef != 0) {
                    $userRef = User::find($userRef);

                    $refPhone = $userRef->phone;
                    $refName = $userRef->fullname;

                    # Referer Current Point and Update New Points
                    $refPoints = $userRef->current_points;
                    $userRef->current_points = $refPoints + 500;

                    # Get referer Points earned by Inviting
                    $invPoints = $userRef->points_earned_by_inv;
                    $userRef->points_earned_by_inv = $invPoints + 500;

                    # Save the Updated Records
                    $userRef->save();

                    # Notify Referer
                    $message = "Hello $refName, your Referral just made a boost and you have received 100 point.";
                    _sendSmsNotification($refPhone, $message);

                }

                # Subscribe the User
                $sub = new Subscription;
                $sub->user_id = $user->id;
                $sub->day_count = 1;
                $sub->day_count_expired = 8;
                $sub->save();

                return response()->json(
                    [
                        'status' => 'success',
                        'response' => 'Subscribed Successfully',
                        'days' => 8
                    ]
                );

            } else if ($koboAmount == 220099) {
                if ($userRef != 0) {
                    $userRef = User::find($userRef);

                    $refPhone = $userRef->phone;
                    $refName = $userRef->fullname;

                    # Referer Current Point and Update New Points
                    $refPoints = $userRef->current_points;
                    $userRef->current_points = $refPoints + 1000;

                    # Get referer Points earned by Inviting
                    $invPoints = $userRef->points_earned_by_inv;
                    $userRef->points_earned_by_inv = $invPoints + 1000;

                    # Save the Updated Records
                    $userRef->save();

                    # Notify Referer
                    $message = "Hello $refName, your Referral just made a boost and you have received 100 point.";
                    _sendSmsNotification($refPhone, $message);
                }

                # Subscribe the User
                $sub = new Subscription;
                $sub->user_id = $user->id;
                $sub->day_count = 1;
                $sub->day_count_expired = 31;
                $sub->save();

                return response()->json(
                    [
                        'status' => 'success',
                        'response' => 'Subscribed Successfully',
                        'days' => 31
                    ]
                );

            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'response' => 'Invalid Kobo Amount Entered'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'response' => 'You already have an active subscription'
                ]
            );
        }

    }
}
