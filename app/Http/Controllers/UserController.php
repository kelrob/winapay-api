<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Validator;
use App\UnattendedUsers;
use App\Withdrawal;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        # Possible error messages
        $errorMessages = [
            'fullname.required' => 'Full Name is required',
            'fullname.min' => 'Full Name can not be less than 6 characters',
            'username.required' => 'Username is required',
            'username.unique' => 'Username has already been taken',
            'phone.required' => 'Phone number is required',
            'phone.unique' => 'Phone number has already been taken',
            'email.required' => 'Email is required',
            'email.email' => 'Invalid email address passed',
            'email.unique' => 'Email has already been taken',
            'password.required' => 'Password is required',
            'password_again.same' => 'Both Password does not match',
            'country.required' => 'Country is required',
            'gender.required' => 'Gender is required',
            'dob.required' => 'Date of birth is required',
            'my_ref.required' => 'Referral is required'
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'fullname' => 'required|min:6',
                'username' => 'required|unique:users',
                'phone' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'password_again' => 'required|same:password',
                'country' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                'my_ref' => 'required',
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Account Creation failed',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }

        # Exclude The Password Again field from going to db
        $request->offsetUnset('password_again');

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        $token =  $user->createToken('MyWinApay')-> accessToken;
        return response()->json(
            [
                'status' => 'success',
                'response' => 'Account Created Successfully',
                'token' => $token,
                'data' => array(
                    'fullname' => $request->fullname,
                    'username' => $request->username,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'country' => $request->country,
                    'gender' => $request->gender,
                    'dob' => $request->dob,
                    'my_ref' => $request->my_ref,
                ),
                'user_id' => $user->id
            ],
            $this-> successStatus
        );
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login() {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $token =  $user->createToken('MyWinApay')-> accessToken;
            return response()->json(
                [
                    'status' => 'success',
                    'token' => $token,
                    'data' => $user
                ],
                $this-> successStatus
            );
        } else{
            return response()->json(['error'=>'Incorrect Login Credentials'], 401);
        }
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loggedInUser()
    {
        $user = Auth::user();
        return response()->json(
            [
                'status' => 'success',
                'data' => $user
            ], $this-> successStatus
        );

    }

    public function ForgotPassword(Request $request) {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        $randomString = _randomString();
        if ($user === null) {

            return response()->json([
                'status' => 'failed',
                'message' => 'Email not found'
            ]);

        } else {
            $user->password_token = $randomString;

            if ($user->save()) {
                # Send the Mail
                //step1
                $cSession = curl_init();
                //step2
                curl_setopt($cSession,CURLOPT_URL,"http://winapay.net/api/v-2-1/password-mail.php?email=$email&token=$randomString");
                curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($cSession,CURLOPT_HEADER, false);
                //step3
                $result = curl_exec($cSession);
                //step4
                curl_close($cSession);
                //step5

                return response()->json([
                    'status' => 'success',
                    'message' => 'Link sent to Email'
                ]);

            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Server Error'
                ]);
            }

        }
    }

    /**
     * Change or Upload User Avatar
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeAvatar(Request $request) {
        $user = Auth::user();
        $userId = $user->id;
        $loggedInUser = User::find($userId);

        $image = $request->file('avatar');


        # Possible error messages
        $errorMessages = [
            'avatar.mimes' => 'Only Jpeg, Jpg and Png Images are allowed',
            'avatar.required' => 'Image can not be empty'
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'avatar' => 'mimes:jpeg,jpg,png|required'
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Error with Images',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }

        # Set Up directory to upload Image
        $dir =  $_SERVER['DOCUMENT_ROOT'];
        $dir = str_replace("api/public","public_html", $dir);
        $original_directory = "$dir/img/avatar";
        $month_year = strtolower(date('F-Y'));
        $target_directory = $original_directory . '/' . $month_year;

        # Check if directory exist or make one
        if (!is_dir($target_directory)) {
            File::makeDirectory($target_directory, $mode = 0777, true, true);
        }

        $filename = rand(1000,1000000) . $image->getClientOriginalName();
        $dbAvatarName = $month_year . '/' .$filename;

        # First image Resize
        $image_resize_100 = Image::make($image->getRealPath());
        /**  Maintain Aspect Ration
        $image_resize_100->resize(100, 100, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        });*/
        $image_resize_100->resize(100, 100);
        $image_resize_100->save($target_directory . '/100x100/'. $filename);

        # Second image Resize
        $image_resize_45 = Image::make($image->getRealPath());
        $image_resize_45->resize(45, 45);
        $image_resize_45->save($target_directory . '/45x45/'. $filename);

        # Third image Resize
        $image_resize_24 = Image::make($image->getRealPath());
        $image_resize_24->resize(24, 24);
        $image_resize_24->save($target_directory . '/24x24/'. $filename);

        # Fourth image Resize
        $image_resize_14 = Image::make($image->getRealPath());
        $image_resize_14->resize(14, 14);
        $image_resize_14->save($target_directory . '/14x14/'. $filename);

        # Upload Original Image
        $uploadFile = $image->move($target_directory, $filename);


        if ($uploadFile) {
            $loggedInUser->avatar = $dbAvatarName;
            $loggedInUser->save();
            return response()->json(
                [
                    'status' => 'success',
                    'response' => 'Avatar changed Successfully',
                    'image_location' => 'https://winapay.com/img/avatar/' . $dbAvatarName
                ]
            );

        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'response' => 'Unable to upload Image at this time',
                    'image_location' => 'https://winapay.com/img/avatar/' . $dbAvatarName
                ]
            );
        }
    }

    public function increaseUserWallet(Request $request) {
        $user  = Auth::user();

        $value = $request->value;
        $currentWallet = $user->wallet;
        $newWallet = $currentWallet + $value;

        $user->wallet = $newWallet;
        if ($user->save()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Wallet Increased Successfully',
                'new_wallet' => $newWallet
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Internal server error'
            ]);
        }
    }

    public function newDeposit(Request $request) {
        $errorMessages = [
            'user_id.required' => 'Please pass a User ID',
            'depositor_name.required' => 'Please supply depositor name',
            'amount.required' => 'Please supply amount deposited',
            'amount.integer' => 'Amount must be a Number',
            'date_of_payment.required' => 'Please Enter Date of Payment',
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'depositor_name' => 'required',
                'amount' => 'required|integer',
                'date_of_payment' => 'required',
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Error with Fields Supplied',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }

        $input = $request->all();
        UnattendedUsers::create($input);

        return response()->json([
            'status' => 'success',
            'message' => 'Your Deposit has been recorded. Your wallet will be credited as soon as payment is confirmed'
        ]);
    }

    public function updateUser(Request $request) {
        $user = Auth::user();

        $field = $request->field;
        $value = $request->value;
        $loggedUser = User::find($user->id);

        $loggedUser->$field = $value;

        $loggedUser->save();

        return response()->json([
            'status' => 'success',
            'message' => 'column ' . $field . ' updated successfully'
        ]);
    }

    public function referralLink() {
        $user = Auth::user();
        return response()->json(
            [
                'status' => 'success',
                'referral_link' => 'https://winapay.com/register?inv=' . $user->id
            ]
        );
    }

    public function processWithdrawal(Request $request) {
        # Logged User Info
        $user = Auth::user();

        # Amount to Withdraw
        $amount = $request->amount;

        # Check if user has filled bank details has been filled
        if (app(BankDetailsController::class)->checkExist($user->id)->getData()->bank_details_exist == true) {

            # Check if user bank details is already verified
            if (app(BankDetailsController::class)->bankDetailsVerified($user->id)->getData()->bank_details_verified == true) {

                if ($amount >= 2000) {

                    # Check if user has a pending withdrawal request
                    if (app(BankDetailsController::class)->userHasPendingWithdrawal($user->id)->getData()->pending_withdrawal == false) {

                        # Check user has Sufficient balance
                        if ($user->wallet >= $amount) {

                            $newWithdrawal = [
                                'user_id' => $user->id,
                                'amount' => $amount,
                                'status' => 0
                            ];

                            Withdrawal::create($newWithdrawal);
                            return response()->json(
                                [
                                    'status' => 'success',
                                    'message' => 'Withdrawal Request Submitted Successfully. Your Account will be credited within 24 hours'
                                ]
                            );

                        } else {
                            return response()->json([
                                'status' => 'failed',
                                'message' => 'Insufficient Funds'
                            ]);
                        }

                    } else {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'You already have a pending withdrawal request'
                        ]);
                    }

                } else {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'message' => 'Minimum Amount to withdraw is N2,000'
                        ]
                    );
                }

            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Kindly Verify your bank details'
                ]);
            }

        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Please fill in your bank details to make a withdrawal request'
            ]);
        }
    }
}