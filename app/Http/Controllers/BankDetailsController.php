<?php

namespace App\Http\Controllers;

use App\BankDetails;
use App\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankDetailsController extends Controller
{
    public function checkExist($user_id) {
        $user = Auth::user();
        $bankInfo = BankDetails::where('user_id', $user_id)->count();

        if ($bankInfo > 0) {
            return response()->json([
                'status' => 'success',
                'bank_details_exist' => true
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'bank_details_exist' => false
            ]);
        }
    }

    public function bankDetailsVerified($user_id) {
        $verified = BankDetails::where('user_id', $user_id)->first();

        if ($verified->uploaded != 0) {
            return response()->json([
                'status' => 'success',
                'bank_details_verified' => true
            ]);
        } else if ($verified->uploaded == 0){
            return response()->json([
                'status' => 'success',
                'bank_details_verified' => false
            ]);
        }
    }

    public function userHasPendingWithdrawal($user_id) {
        $status = Withdrawal::where('user_id', $user_id)
            ->where('status', 0)->count();

        if ($status == 0) {
            return response()->json([
                'status' => 'success',
                'pending_withdrawal' => false
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'pending_withdrawal' => true
            ]);
        }
    }

    public function newBankDetails(Request $request) {
        $user = Auth::user();

        $data = [
            'user_id' => $user->id,
            'account_name' => strtoupper($request->account_name),
            'bank_name' => $request->bank_name,
            'account_number' => $request->account_number,
            'type' => $request->type,
            'uploaded' => 0
        ];

        BankDetails::create($data);

        return response()->json(
            [
                'status' => 'success',
                'message' => 'Bank details uploaded successfully'
            ]
        );
    }

    public function verifyBankDetails(Request $request) {
        $bvn = $request->bvn;
        //step1
        $cSession = curl_init();
        $authorization = "Authorization: Bearer sk_test_55f94c6959319aa3bfc4ae91de12e1dbb7a1d901";
        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($cSession,CURLOPT_URL,"https://api.paystack.co/bank/resolve_bvn/$bvn");
        curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($cSession,CURLOPT_HEADER, false);

        //step3
        $result = curl_exec($cSession);

        //step4
        curl_close($cSession);

        //step5
        $array = $result;

        return $result;
    }

    public function approveBankDetails() {
        $user = Auth::user();

        $userBankDetails = BankDetails::where('user_id', $user->id)->update([
            'uploaded' => 1
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Bank details completely verified'
        ]);
    }

}
