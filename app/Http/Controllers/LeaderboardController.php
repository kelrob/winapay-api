<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class LeaderboardController extends Controller
{
    public function getDailyLeaderBoard() {
        $dailyLeaderboard = DB::table('daily_leaderboard')
            ->select(DB::raw('user_id, SUM(points) AS total_points'))
            ->groupBy('user_id')
            ->orderBy('total_points', 'desc')
            ->limit(20)
            ->get();

        $data = [];
        foreach ($dailyLeaderboard as $rank) {
            $data[] = [
                'points' => $rank->total_points,
                'user_id' => $rank->user_id,
                'user_id_info' => User::find($rank->user_id)
            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'leaderboard' => $data
            ]
        );
    }

    public function getMonthlyLeaderBoard() {
        $monthlyLeaderboard = DB::table('monthly_leaderboard')
            ->select(DB::raw('user_id, SUM(points) AS total_points'))
            ->groupBy('user_id')
            ->orderBy('total_points', 'desc')
            ->limit(20)
            ->get();

        $data = [];
        foreach ($monthlyLeaderboard as $rank) {
            $data[] = [
                'points' => $rank->total_points,
                'user_id' => $rank->user_id,
                'user_id_info' => User::find($rank->user_id)
            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'leaderboard' => $data
            ]
        );

    }

    public function readTop1() {
        $monthlyLeaderboard = DB::table('monthly_leaderboard')
            ->select(DB::raw('user_id, SUM(points) AS total_points'))
            ->groupBy('user_id')
            ->orderBy('total_points', 'desc')
            ->limit(1)
            ->get();

        $data = [];
        foreach ($monthlyLeaderboard as $rank) {
            $data[] = [
                'points' => $rank->total_points,
                'user_id' => $rank->user_id,
                'user_id_info' => User::find($rank->user_id)
            ];
        }

        return $data;
    }

    public function readTop2to5() {
        $monthlyLeaderboard = DB::table('monthly_leaderboard')
            ->select(DB::raw('user_id, SUM(points) AS total_points'))
            ->groupBy('user_id')
            ->orderBy('total_points', 'desc')
            ->offset(1)
            ->limit(4)
            ->get();

        $data = [];
        foreach ($monthlyLeaderboard as $rank) {
            $data[] = [
                'points' => $rank->total_points,
                'user_id' => $rank->user_id,
                'user_id_info' => User::find($rank->user_id)
            ];
        }

        return $data;
    }
}
