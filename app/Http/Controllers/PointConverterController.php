<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PointConverterController extends Controller
{
    public function convertPoint(Request $request) {
        $user = Auth::user();
        $pointsEntered = $request->points;
        $currentDay = date('d');

        # Check if User Can convert
        $boostActive = app(SubscriptionController::class)->subActive($user->id);
        $boostResponse = $boostActive->getData();
        if ($boostResponse->active == true) {
            # Make Sure Point Converter is opened on specific days
            if (1 == 1) {
                if ($pointsEntered > 20000 || $pointsEntered < 10000) {
                    return response()->json(
                        [
                            'status' => 'failed',
                            'message' => 'Points can not be more than 20,000 and Lesser than 10,000.',
                        ], 401
                    );
                } else {
                    if ($pointsEntered <= $user->current_points) {

                        # Get the Top 1 query
                        $topOneQuery = app(LeaderboardController::class)->readTop1();
                        $topOne = false;
                        $userId = ($topOneQuery[0]['user_id_info']['id']);

                        if ($user->id == $userId) {
                            $topOne = true;
                        }

                        if ($topOne == true) {
                            $value = 3;
                        }

                        # Get the Top 2 - 5 query
                        $top2to5Query = app(LeaderboardController::class)->readTop2to5();
                        $top2to5 = false;
                        foreach ($top2to5Query as $keys) {
                            $userId =  $keys['user_id_info']['id'];
                            if ($userId == $user->id) {
                                $top2to5 = true;
                            }
                        }

                        if ($top2to5 == true) {
                            $value = 5;
                        }

                        if (!isset($value)) {
                            $value = 10;
                        }

                        # Start Converting the points
                        $convertedValue = $pointsEntered / $value;
                        $amountToAdd =  number_format((float) $convertedValue, 2, '.', '');

                        # Deduct The Points from user already Existing Points
                        $loggedInUser = User::find($user->id);
                        $loggedInUser->current_points = $loggedInUser->current_points - $pointsEntered;

                        # Add to the user already existing user wallet
                        $loggedInUser->wallet = $loggedInUser->wallet + $amountToAdd;

                        # Save
                        $loggedInUser->save();

                        return response()->json([
                            'status' => 'success',
                            'message' => 'Point Converted to Cash Successfully',
                            'amount_gotten' => $amountToAdd
                        ]);
                    } else {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'You do not have up to ' . $pointsEntered . ' points to convert'
                        ]);
                    }
                }
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'You do not have an active Subscription to convert point to cash'
                ]
            );
        }

    }
}
