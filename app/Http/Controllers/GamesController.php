<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UsersGames;
use App\GamesPlayed;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\TimeSpent;

class GamesController extends Controller
{
    public $successStatus = 200;

    public function userGames($user_id) {
        $user = Auth::user();

        $userGames = UsersGames::where('user_id', $user_id)
            ->orderBy('time_created', 'desc')
            ->paginate(15);

        $data = [];
        foreach ($userGames as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
            ], $this-> successStatus
        );
    }

    public function getGame($game_id) {
        $user = Auth::user();
        $games = UsersGames::where('id', $game_id)->get();

        $data = [];
        foreach ($games as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,
                'user_id'           => $game->user_id,
                'user_id_info'      => User::find($game->user_id)

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
            ], $this-> successStatus
        );

    }

    public function allGames() {
        $user = Auth::user();
        $allGames = UsersGames::where('game_status', 1)
            ->where('approved', 1)
            ->where('game_expired', 0)
            ->orderBy('id', 'desc')
            ->paginate(15);

        $data = [];
        foreach ($allGames as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,
                'user_id'           => $game->user_id,
                'user_id_info'      => User::find($game->user_id)

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
                'pagination' => array(
                    'current_page' => $allGames->currentPage(),
                    'last_page' => $allGames->lastPage(),
                    'per_page' => $allGames->perPage(),
                    'next_page_url' => $allGames->nextPageUrl(),
                    'prev_page_url' => $allGames->previousPageUrl(),
                    'total' => $allGames->total()
                )
            ], $this-> successStatus
        );
    }

    public function allFreeGames() {
        $user = Auth::user();
        $freeGames = UsersGames::where('game_status', 1)
            ->where('game_package', 'free_game')
            ->where('approved', 1)
            ->where('game_expired', 0)
            ->orderBy('id', 'desc')
            ->paginate(15);

        $data = [];
        foreach ($freeGames as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,
                'user_id'           => $game->user_id,
                'user_id_info'      => User::find($game->user_id)

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
                'pagination' => array(
                    'current_page' => $freeGames->currentPage(),
                    'last_page' => $freeGames->lastPage(),
                    'per_page' => $freeGames->perPage(),
                    'next_page_url' => $freeGames->nextPageUrl(),
                    'prev_page_url' => $freeGames->previousPageUrl(),
                    'total' => $freeGames->total()
                )
            ], $this-> successStatus
        );
    }


    public function allPaidGames() {
        $user = Auth::user();
        $paidGames = UsersGames::where('game_status', 1)
            ->where('game_package', 'paid_game')
            ->where('approved', 1)
            ->where('game_expired', 0)
            ->orderBy('id', 'desc')
            ->paginate(15);

        $data = [];
        foreach ($paidGames as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,
                'user_id'           => $game->user_id,
                'user_id_info'      => User::find($game->user_id)

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
                'pagination' => array(
                    'current_page' => $paidGames->currentPage(),
                    'last_page' => $paidGames->lastPage(),
                    'per_page' => $paidGames->perPage(),
                    'next_page_url' => $paidGames->nextPageUrl(),
                    'prev_page_url' => $paidGames->previousPageUrl(),
                    'total' => $paidGames->total()
                )
            ], $this-> successStatus
        );
    }

    public function increaseGameView($game_id) {
        $user  = Auth::user();

        $game = UsersGames::find($game_id);
        $currentView = $game->total_viewed;
        $newView = $currentView + 1;

        $game->total_viewed = $newView;
        if ($game->save()) {
            return response()->json([
                'status' => 'success',
                'message' => 'Game View Increased'
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'Internal server error'
            ]);
        }
    }

    public function userPlayedGame(Request $request) {

        # Possible error messages
        $errorMessages = [
            'game_id.required' => 'Game Id is required',
            'game_package.required' => 'Game Package is Required'
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'game_id' => 'required',
                'game_package' => 'required',
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Fill all required field',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }

        $user = Auth::user();
        $gamePlayed = new GamesPlayed;

        $gamePlayed->user_id = $user->id;
        $gamePlayed->game_id = $request->game_id;
        $gamePlayed->points = 0;
        $gamePlayed->game_package = $request->game_package;

        $userPlayed = GamesPlayed::where('game_id', $request->game_id)
            ->where('user_id', $user->id)
            ->first();

        if ($userPlayed === null) {
            if ($gamePlayed->save()) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Game Played recorded'
                ]);
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'User has played this game already'
            ]);
        }

    }

    public function createGame(Request $request) {

        $user = Auth::user();
        $userWallet = $user->wallet;
        $game = new UsersGames;

        $image = $request->file('featured_image');

        # Possible error messages
        $errorMessages = [
            'featured_image.mimes' => 'Only Jpeg, Jpg and Png Images are allowed',
            'featured_image.required' => 'Image can not be empty'
        ];

        # Validation rules for all Input fields
        $validator = Validator::make($request->all(),
            [
                'featured_image' => 'mimes:jpeg,jpg,png|required'
            ], $errorMessages
        );

        # Check if Validation fails
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => 'failed',
                    'message' => 'Error with Images',
                    'errors' => $validator->errors()->all()
                ], 401
            );
        }
 
        # Featured Image
        $filename = rand(1000,1000000) . $image->getClientOriginalName();
        $game->time_created = time();
        $game->time_updated = time();
        $game->alert = 0;
        $game->game_status = 0;
        $game->game_expired = 0;
        $game->approved = 0;
        $game->up_votes = 0;
        $game->down_votes = 0;
        $game->admin_ignore = 0;
        $game->total_viewed = 0;
        $game->tracking_key = _trackingKey();
        $game->month_year = strtolower(date('F-Y'));
        $game->featured_image = strtolower($game->month_year . '/' . $filename);
        $game->user_id = $user->id;
        $game->timer = $request->timer;
        $game->answer = $request->answer;
        $game->question = $request->question;
        $game->option_a = $request->option_a;
        $game->option_b = $request->option_b;
        $game->option_c = $request->option_c;
        $game->option_d = $request->option_d;
        $game->category = $request->category;
        $game->video_desc = nl2br($request->video_desc);
        $game->game_type = $request->game_type;
        $game->game_winners = $request->game_winners;
        $game->game_title = $request->game_title;
        $game->game_price = $request->game_price;
        $game->game_package = $request->game_package;
        $game->embedded_url = $request->embedded_url;
        $game->post_write_up = nl2br($request->post_write_up);
        $game->game_winner_price = $request->game_winner_price;
        $game->initial_game_package = $request->game_package;
        $game->permalink = _createPermalink(strtolower($request->game_title));

        $amountToCharge = $game->game_winner_price - $game->game_price;

        if ($game->game_package == 'paid_game') {
            if ($userWallet < $amountToCharge) {
                return response()->json(
                    [
                        'status' => 'failed',
                        'message' => 'Insufficient Amount in wallet to create game'
                    ], 401
                );
            } else {
                $loggedInUser = User::find($user->id);
                $loggedInUser->wallet = $userWallet - $amountToCharge;
                $loggedInUser->save();
            }
        }

        # Set Up Directory to upload featured image
        $dir =  $_SERVER['DOCUMENT_ROOT'];
        $dir = str_replace("api/public","public_html", $dir);
        $original_directory = "$dir/img/games";
        $month_year = strtolower(date('F-Y'));
        $target_directory = $original_directory . '/' . $month_year;

        # Check if directory exist or make one
        if (!is_dir($target_directory)) {
            File::makeDirectory($target_directory, $mode = 0777, true, true);
        }

        $resized_directory = $original_directory . '/' . $month_year . '/resized';
        $resized_directory2 = $original_directory . '/' . $month_year . '/resized2';

        if (!is_dir($resized_directory)) {
            mkdir($resized_directory, 0777, true);
        }

        if (!is_dir($resized_directory2)) {
            mkdir($resized_directory2, 0777, true);
        }

        # First image Resize
        $image_resize_1 = Image::make($image->getRealPath());
        $image_resize_1->resize(210, 118);
        $image_resize_1->save($resized_directory . '/'. $filename);

        # Second image Resize
        $image_resize_2 = Image::make($image->getRealPath());
        $image_resize_2->resize(100, 75);
        $image_resize_2->save($resized_directory2 . '/'. $filename);

        # Upload Original Image
        $uploadFile = $image->move($target_directory, $filename);

        if ($uploadFile) {
            if ($game->save()) {
                return response()->json(
                    [
                        'status' => 'success',
                        'response' => 'Game Created Successfully',
                        'game_info' => $game
                    ], 200
                );
            } else {
                return response()->json(
                    [
                        'status' => 'failed',
                        'response' => 'There was an error creating this game'
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'response' => 'There was an error with image upload',
                ]
            );
        }
    }

    public function totalPlay($game_id) {
        $result = GamesPlayed::where('game_id', $game_id)->get();

        return response()->json([
            'status' => 'success',
            'total_play' => $result->count()
        ]);
    }

    public function searchGame($search_term) {
        $searchTerm = _escapeLike($search_term);

        $result = UsersGames::where('game_title', 'like', '%' . $searchTerm . '%')
            ->orWhere('category', 'like', '%' . $searchTerm . '%')
            ->where(function ($q) {
                $q->where('game_status', 1)
                    ->where('game_expired', 0)
                    ->where('user_id', '!=', 81988);
            })->paginate(10);

        $data = [];
        foreach ($result as $game) {
            $data[] = [
                'game_id'           => $game->id,
                'game_title'        => $game->game_title,
                'category'          => $game->category,
                'featured_image'    => 'https://winapay.com/img/games/' . $game->featured_image,
                'game_type'         => $game->game_type,
                'post_write_up'     => $game->post_write_up,
                'permalink'         => 'https://winapay.com/games/' . $game->permalink,
                'embedded_url'      => $game->embedded_url,
                'video_desc'        => $game->video_desc,
                'question'          => json_decode($game->question),
                'option_a'          => json_decode($game->option_a),
                'option_b'          => json_decode($game->option_b),
                'option_c'          => json_decode($game->option_c),
                'option_d'          => json_decode($game->option_d),
                'answer'            => json_decode($game->answer),
                'game_package'      => $game->game_package,
                'timer'             => $game->timer,
                'game_price'        => $game->game_price,
                'game_winner_price' => $game->game_winner_price,
                'game_winners'      => $game->game_winners,
                'game_status'       => $game->game_status,
                'game_expired'      => $game->game_expired,
                'approved'          => $game->approved,
                'up_votes'          => $game->up_votes,
                'down_votes'        => $game->down_votes,
                'total_viewed'      => $game->total_viewed,
                'total_played'      => $game->total_played,
                'time_created'      => $game->time_created,
                'time_updated'      => $game->time_updated,
                'month_year'        => $game->month_year,
                'alert'             => $game->alert,
                'admin_ignore'      => $game->admin_ignore,
                'initial_game_package' => $game->initial_game_package,
                'user_id'           => $game->user_id,
                'user_id_info'      => User::find($game->user_id)

            ];
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
                'pagination' => array(
                    'current_page' => $result->currentPage(),
                    'last_page' => $result->lastPage(),
                    'per_page' => $result->perPage(),
                    'next_page_url' => $result->nextPageUrl(),
                    'prev_page_url' => $result->previousPageUrl(),
                    'total' => $result->total()
                )
            ], $this-> successStatus
        );
    }


    public function gamesPlayed($user_id) {
        $playedGames = GamesPlayed::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->paginate(15);

        $data = [];
        foreach ($playedGames as $games) {
            $data[] = UsersGames::find($games->game_id);
        }

        return response()->json(
            [
                'status' => 'success',
                'game_info' => $data,
                'pagination' => array(
                    'current_page' => $playedGames->currentPage(),
                    'last_page' => $playedGames->lastPage(),
                    'per_page' => $playedGames->perPage(),
                    'next_page_url' => $playedGames->nextPageUrl(),
                    'prev_page_url' => $playedGames->previousPageUrl(),
                    'total' => $playedGames->total()
                )
            ], $this-> successStatus
        );
    }

    public function recordGameScore(Request $request) {
        $user = Auth::user();

        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['time_spent'] = $request->end_time - $request->start_time;

        TimeSpent::create($input);

        return response()->json([
            'status' => 'success',
            'message' => 'Game Play recorded for this user'
        ]);
    }

    public function closeGame($game_id) {
        $user = Auth::user();
        $game = UsersGames::find($game_id);

        if ($user->id == $game->user_id) {
            $game->game_status = 0;
            $game->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Game closed Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'You do not have permission to cliose this game'
            ]);
        }
    }

    public function openGame($game_id) {
        $user = Auth::user();
        $game = UsersGames::find($game_id);

        if ($user->id == $game->user_id) {
            $game->game_status = 1;
            $game->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Game Opened Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'You do not have permission to cliose this game'
            ]);
        }
    }

    public function editGame(Request $request, $game_id) {
        $user = Auth::user();
        if (!$request['featured_image']) {
            $game = UsersGames::where('id', $game_id)->first();
            $ownerId = $game->id;

            if ($ownerId == $user->id) {
                $game->update($request->all());
                if ($game->save()) {
                    return response()->json([
                        'status' => 'success',
                        'game' => $game
                    ]);
                } else {
                    return response()->json([
                        'status' => 'failed',
                        'game' => $game
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'You do not have access to edit this game'
                ]);
            }

        } else {
            return response()->json(['status' => 'failed', 'message' => 'You can not update featured_image with this endpoint']);
        }
    }
}
