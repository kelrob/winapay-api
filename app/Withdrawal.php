<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    protected $table = 'withdrawal_request';
    protected $guarded = ['id'];

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;
}
