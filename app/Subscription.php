<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    protected $guarded = ['id'];
    public $timestamps = false;

    const CREATED_AT = 'time_created';
    const UPDATED_AT = null;
}
